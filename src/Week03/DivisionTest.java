package Week03;
// Import Scanner
import java.util.Scanner;

//Declare Public Class
public class DivisionTest {

    public static void main(String[] args) {

        // Get a new scanner for user inputs
        Scanner scanner = new Scanner(System. in );

        while (true) {
            try {
                // Get User dividend or Numerator
                System.out.print("Please enter numerator: ");
                int d = scanner.nextInt();
                // Get User divisor or Denominator
                System.out.print("Please enter denominator: ");
                int e = scanner.nextInt();
                int results = d / e;
                // show solution
                System.out.println("Solution: " + d + "/" + e + " = " + results);
                // Zeroes excluded, end the code.
                break;
                // Loop exception
            } catch(Exception ae) {
                System.out.println("Division by zero cannot occur");
                // Continue the loop
                continue;
            }
        }
    }
}